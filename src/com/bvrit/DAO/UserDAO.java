package com.bvrit.DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.bvrith.model.E_UserBean;
import com.bvrith.model.ResumeBean;
import com.bvrith.model.UserBean;

//import com.sun.crypto.provider.RSACipher;

public class UserDAO {
	Statement st=null;
	Connection conn = null;
	
	public UserDAO() throws SQLException, ClassNotFoundException, IOException, NamingException{
		ConnectionDAO connDAO = new ConnectionDAO();
		 conn = connDAO.getConnection();
		 st = conn.createStatement();
	}

	public int createUser(UserBean uBean) {
		int result = 0;
		String email = uBean.getEmail();
        String password = uBean.getPassword();
        String name = uBean.getName();
        String gender = uBean.getGender();
        String location = uBean.getLocation();
        String phone = uBean.getPhone();
        String workexp =uBean.getWorkexp();
        String skills = uBean.getSkills();
        String edu = uBean.getEdu();
        String image = uBean.getImage();
        System.out.println("in create user");
        
		String query = "insert into jobseeker values('"+email+"','"+password+"','"+name+"','"+gender+"','"+location+"','"+phone+"','"+workexp+"','"+skills+"','"+edu+"','"+image+"')";
			       System.out.println("in create user");
	
		System.out.println(st);
            //execute query
		 try {
			result = st.executeUpdate(query);
			System.out.println("i am on");
			
		} catch (SQLException e1) {
			result = 0;
			
			e1.printStackTrace();
		}  
           try{ 
            if(conn != null){
                conn.close();
                }
            if(st != null){
            	st.close();
            	}
           }
           catch(Exception e){
        	   e.printStackTrace();
           }
            System.out.println(result);
            return result;
 
	}

	public boolean validateUser(UserBean uBean) throws SQLException{
		String email = uBean.getEmail();
        String password = uBean.getPassword();
		ResultSet rs = null;			
		String sql = "SELECT * FROM jobseeker";					
	    rs = st.executeQuery(sql);
        while(rs.next()){
        	String em = rs.getString("email");
	        String ps = rs.getString("password");
	        if(email.equals(em) && password.equals(ps)){
	        	conn.close();
		        st.close();
		        rs.close();
		        return true;
	        }
	    }
		return false;	      
	}
	
	public int updateUser(String email,String pwd1,String pwd2) throws SQLException{	
			String query = "update jobseeker set password='"+pwd2+"' where email='"+email+"' && password='"+pwd1+"';";		       	
	            int result = st.executeUpdate(query);
	            if(result>=1){
	                System.out.println("Record updated");
	            }else{
	                System.out.println("Record not updated");
	                conn.close();
	                st.close();
	                return 0;
	            }
	            conn.close();
                st.close();
	            return 1;	   	       	    						
	}
	
	public int updateEmail(String email,String pwd1,String pwd2) throws SQLException{	
		String query = "update employer set password='"+pwd2+"' where email='"+email+"' && password='"+pwd1+"';";		       	
            int result = st.executeUpdate(query);
            if(result>=1){
                System.out.println("Record updated");
            }else{
                System.out.println("Record not updated");
                conn.close();
                st.close();
                return 0;
            }
            conn.close();
            st.close();
            return 1;	   	       	    						
}

	public List<UserBean> listProfile(String email1) throws SQLException,ClassNotFoundException{
		List<UserBean> conCollection = new ArrayList<UserBean>();
	
		String query = "SELECT * FROM jobseeker where email='"+email1+"'";
		ResultSet result = st.executeQuery(query);
		UserBean userBean2;
		while(result.next()){
	           //Retrieve by column name
	          
	           String  email= result.getString("email");
	           String  password = result.getString("password");
	           String  name= result.getString("name");
	           String  gender = result.getString("gender");
	           String  location = result.getString("location");
	           String  phone = result.getString("phone");
	           String  workexp = result.getString("workexp");
	           String  skills = result.getString("skills");
	           String  edu = result.getString("edu");
	           String  image = result.getString("image");
	           userBean2 = new UserBean(email, password, name, gender, location, phone, workexp, skills, edu,image);
	           
	           conCollection.add(userBean2);
		}
		System.out.println(conCollection.size());
		return conCollection;
	}
	
	
	/*public List<String> listresume() throws SQLException,ClassNotFoundException{
		List<String> conCollection = new ArrayList<String>();
		String query = "SELECT resume FROM jobseeker";
		ResultSet result = st.executeQuery(query);
		UserBean userBean2;
		while(result.next()){
	           //Retrieve by column name
          
	          String  email= result.getString("email");
	           String  password = result.getString("password");
	           String  name= result.getString("name");
	           String  gender = result.getString("gender");
	           String  location = result.getString("location");
	           String  phone = result.getString("phone");
	           String  workexp = result.getString("workexp");
	           String  skills = result.getString("skills");
	           String  edu = result.getString("edu");
	           String  resume= result.getString("resume");
	          // userBean2 = new UserBean(email, password, resume, name, gender, location, phone, workexp, skills, edu);
	           conCollection.add(resume);
		}
		System.out.println(conCollection.size());
		
		return conCollection;
	}
	
	*/
	
	public int updateProfile(UserBean uBean) throws SQLException{	
		
		 int result;
	        String email = uBean.getEmail();
	        String name = uBean.getName();
	        String location = uBean.getLocation();
	        String phone = uBean.getPhone();
	        String workexp = uBean.getWorkexp();
	        String skills = uBean.getSkills();
	        String edu = uBean.getEdu();

		String query = "UPDATE jobseeker set name ='"+name+"',location='"+location+"',phone='"+phone+"',workexp='"+workexp+"',skills='"+skills+"',edu='"+edu+"' where email='"+email+"'";		       	
            result = st.executeUpdate(query);
            if(result>=1){
                System.out.println("Profile updated");
            }else{
                System.out.println("Profile not updated");
                conn.close();
                st.close();
                result = 0;
            }
            conn.close();
            st.close();
            return result;	   	       	    						
}
	
	
	
	
	
	public int createE_User(E_UserBean euBean) {
		int result = 0;
		String email = euBean.getEmail();
        String password = euBean.getPassword();
        String confpassworrd =euBean.getConfpassworrd();
        String industry = euBean.getIndustry();
        String companyname = euBean.getCompanyname();
        String nation =euBean.getNation();
        String location = euBean.getLocation();
        String phone = euBean.getPhone();

		String query = "insert into employer values('"+email+"','"+password+"','"+confpassworrd+"','"+industry+"','"+companyname+"','"+nation+"','"+location+"','"+phone+"')";
			       
	
		System.out.println(st);
            //execute query
		System.out.println(result);
		
		
		 try {
			result = st.executeUpdate(query);
			
		} catch (SQLException e1) {
			result = 0;
			
			e1.printStackTrace();
		}  
           try{ 
            if(conn != null){
                conn.close();
                }
            if(st != null){
            	st.close();
            	}
           }
           catch(Exception e){
        	   e.printStackTrace();
           }
            System.out.println(result);
            return result;
 
	}
	public boolean validateE_User(E_UserBean euBean) throws SQLException{
		String email = euBean.getEmail();
        String password = euBean.getPassword();
		 	ResultSet rs = null;			
			String sql = "SELECT * FROM employer";					
			System.out.println("connected");
	        rs = st.executeQuery(sql);
	         System.out.println("query excecuted");
	         while(rs.next()){
	        	 String em = rs.getString("email");
	             String ps = rs.getString("password");
	        	 if(email.equals(em) && password.equals(ps)){
	        		 conn.close();
		        	  st.close();
		        	  rs.close();
		        	  return true;
	        	 }
	         }
			return false;	      
	}
	
	public int SetProfilePic(String img,UserBean ubean) throws SQLException{
		String email = ubean.getEmail();
		String query = "update jobseeker set image='"+img+"' where email='"+email+"'";
		//Statement st = conn.createStatement();
		int result = st.executeUpdate(query);
		System.out.println(email);
		System.out.println(query);
		if (result >= 1) {
			System.out.println("Record updated");
		} else {
			System.out.println("Record not updated");
			conn.close();
			st.close();
			return 0;
		}
		conn.close();
		st.close();
		return result;
		
	}
	
	
	public String getProfilePic(String email) throws SQLException{
		String dp =null;
		String query = "select image from jobseeker";
		ResultSet rs = null;
		rs = st.executeQuery(query);
		while (rs.next()) {
			 dp = rs.getString("image");
			
		}
		
		return dp;
		
	}

	/*public int updateEmail(String email, String pwd, String rpwd) throws SQLException {
		String query = "update employeer set password='"+pwd+"' where email='"+email+"' && password='"+rpwd+"';";		       	
        int result = st.executeUpdate(query);
        if(result>=1){
            System.out.println("Record updated");
        }else{
            System.out.println("Record not updated");
            conn.close();
            st.close();
            return 0;
        }
        conn.close();
        st.close();
      	       	   
		// TODO Auto-generated method stub
		return 1;
	}*/

	
}