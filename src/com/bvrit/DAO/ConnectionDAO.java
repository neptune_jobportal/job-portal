
package com.bvrit.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

//import com.online.dao.ConnectionDao;

public class ConnectionDAO{
	Connection conn;
	String url = "jdbc:mysql://localhost:3306/job_portal";
	String user = "root";
	String password = "123456";

	public Connection getConnection() throws ClassNotFoundException, IOException, NamingException{
		
		/*Properties config = new Properties();
		config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
		*/
		//Class.forName("com.mysql.jdbc.Driver");
			//System.out.println("driver loaded");
			
		/*try {
			File file = new File("C:\\Users\\CHINNI\\Desktop\\Eclipse\\Eclipse Folder\\eclipse\\FinalProject\\OnlineExamination\\config.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			Enumeration enuKeys=properties.keys();
			//load driverl
			url = properties.getProperty("url");
			 user = properties.getProperty("user");
			password = properties.getProperty("password");
		}catch(FileNotFoundException e){
			e.printStackTrace();				
		}
		catch (IOException e){
			e.printStackTrace();
		}*/
		

		try {
	    	
			//conn = DriverManager.getConnection(url,user,password);
            //Logger log = Logger.getLogger(ConnectionDao.class);
			//log.info("driver loaded");

			 //System.out.println(conn);
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc:mysql://localhost:3306/job_portal");
			Connection conn = ds.getConnection();
			System.out.println("MySql driver loaded");
			//con = DriverManager.getConnection(URL,USER,PASSWORD);
			System.out.println(conn);
			return conn;

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e);
		}
	   return conn;
	}
	public static void main(String args[]){		
		ConnectionDAO connDAO = new ConnectionDAO();
	}
}