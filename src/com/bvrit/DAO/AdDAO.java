package com.bvrit.DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.bvrith.model.PostaddBean;


public class AdDAO {
private int noOfRecords;
	public int getNoOfRecords(){
		return noOfRecords;
	}
    Statement st=null;
    Connection conn = null;
    PreparedStatement pst;
    
    public AdDAO() throws SQLException, ClassNotFoundException, IOException, NamingException{
         ConnectionDAO postDAO = new ConnectionDAO();
      
         conn = postDAO.getConnection();
         st = conn.createStatement();
         System.out.println("connected to database");
    }

    public int postadd(PostaddBean pbean) throws SQLException{
        String query = "insert into postadd values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        int result=0;
         pst =conn.prepareStatement(query);
         //pst =conn.prepareStatement(query);
         pst.setString(1, pbean.getJobtitle());
         pst.setString(2, pbean.getVacancies());
         pst.setString(3, pbean.getJobdescription());
         pst.setString(4, pbean.getWorkexperience());
         pst.setString(5, pbean.getSalarymin());
         pst.setString(6, pbean.getSalarymax());
         pst.setString(7, pbean.getLocation());
         pst.setString(8, pbean.getIndustry());
         pst.setString(9, pbean.getArea());
         pst.setString(10, pbean.getQualification());
         pst.setString(11, pbean.getCompanyname());
         pst.setString(12, pbean.getContactperson());
         pst.setString(13, pbean.getContactno());
         pst.setString(14, pbean.getEmail());
         pst.setString(15, pbean.getStatus());
         pst.setString(16, pbean.getId());
         result = pst.executeUpdate();
         System.out.println("in dao");
        return result;
       
    }
   
   
    public List<PostaddBean> listadd(int offset , int noOfRecords)throws SQLException{
    	/*String query= "SELECT * from postadd";*/
		String query = "SELECT SQL_CALC_FOUND_ROWS * from postadd limit "+ offset + ", " + noOfRecords;
		System.out.println(query);
		List<PostaddBean> pbeanList = new ArrayList<PostaddBean>();
       pst = conn.prepareStatement(query);
       ResultSet rs = pst.executeQuery();
       PostaddBean pbean;
       while(rs.next()){
    	  System.out.println("i am in list ad"); 
           pbean = new PostaddBean(rs.getString("id"),rs.getString("jobtitle"), rs.getString("vacancies"),rs.getString("jobdescription"),rs.getString("workexperience"),rs.getString("salarymin"),rs.getString("salarymax"),rs.getString("location"),rs.getString("industry"),rs.getString("area"),rs.getString("qualification"),rs.getString("companyname"),rs.getString("contactperson"),rs.getString("contactno"),rs.getString("email"),rs.getString("status"));
           pbeanList.add(pbean);
       }
       
       rs.close();
		
		rs = pst.executeQuery("SELECT FOUND_ROWS()");
		
		if(rs.next())
			this.noOfRecords = rs.getInt(1);
       return pbeanList;
   }
    
    public List<PostaddBean> listE_add(  String email)throws SQLException{
		String query = "SELECT * from postadd where email='"+email+"'";
      List<PostaddBean> pbeanList = new ArrayList<PostaddBean>();
      pst = conn.prepareStatement(query);
      ResultSet rs = pst.executeQuery(query);
      PostaddBean pbean;
      while(rs.next()){
   	   System.out.print("I AM IN WHILE");
          pbean = new PostaddBean(rs.getString("id"),rs.getString("jobtitle"), rs.getString("vacancies"),rs.getString("jobdescription"),rs.getString("workexperience"),rs.getString("salarymin"),rs.getString("salarymax"),rs.getString("location"),rs.getString("industry"),rs.getString("area"),rs.getString("qualification"),rs.getString("companyname"),rs.getString("contactperson"),rs.getString("contactno"),rs.getString("email"),rs.getString("status"));
          pbeanList.add(pbean);
      }
      return pbeanList;
  }
    
/*	public int updateStatus(PostaddBean pBean) throws SQLException{	
		
		 int result;
	        String status = pBean.getStatus();
	        String ID = pBean.getId();
System.out.println("ID");
	    	String query = "UPDATE postadd SET status = 'Vacency closed', where id = '"+ID+"'";
	    	UPDATE table_name
	    	SET column1=value1,column2=value2,...
	    	WHERE some_column=some_value;
	    	
           result = st.executeUpdate(query);
            if(result>=1){
                System.out.println("Status updated");
            }else{
                System.out.println("Status not updated");
                conn.close();
                st.close();
                result = 0;
            }
            conn.close();
            st.close();
			return result;
	}*/
    
    public int updateStatus(String id) throws SQLException {
		int result;
		String query = "update postadd set status = 'Vacency closed' where id = ?";
		System.out.println(id);
		pst = conn.prepareStatement(query);
		pst.setString(1, id);
		result = st.executeUpdate(query);
		return result;
	}
    
    
    
    
}