package com.bvrit.DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.bvrith.model.ResumeBean;
//import com.bvrith.model.UserBean;


public class ResumeDAO {
	
    Statement st=null;
    Connection conn = null;
    PreparedStatement pst;
    
    public ResumeDAO() throws SQLException, ClassNotFoundException, IOException, NamingException{
        ConnectionDAO resumeDAO = new ConnectionDAO();
     
        conn = resumeDAO.getConnection();
        st = conn.createStatement();
        System.out.println("connected to database");
   }

    public int createResume(ResumeBean rbean) throws SQLException{
        String query = "insert into resume values(?,?,?,?,?,?,?,?,?,?,?,?)";
        int result=0;
         pst =conn.prepareStatement(query);
         //pst =conn.prepareStatement(query);
         pst.setString(1,  rbean.getFirstname());
         pst.setString(2,  rbean.getLastname());
         pst.setString(3,  rbean.getLocation());
         pst.setString(4,  rbean.getGender());
         pst.setString(5,  rbean.getPhone());
         pst.setString(6, rbean.getQualification());
         pst.setString(7, rbean.getInstitute());
         pst.setString(8, rbean.getPyear());
         pst.setString(9, rbean.getSkills());
         pst.setString(10,  rbean.getEmail());
         pst.setString(11,  rbean.getExperience());
         pst.setString(12, rbean.getSpecialization());

 

         result = pst.executeUpdate();
         System.out.println("in resume dao");
        return result;
       
    }
    
	public List<ResumeBean> listResume(String email1) throws SQLException,ClassNotFoundException{
		List<ResumeBean> conCollection = new ArrayList<ResumeBean>();
	
		String query =  "SELECT * FROM resume  where email='"+email1+"'";
		ResultSet result = st.executeQuery(query);
		ResumeBean resumeBean2;
		while(result.next()){
	           //Retrieve by column name
	          
			String firstname = result.getString("firstname");
			String lastname = result.getString("lastname");
			String location= result.getString("location");
			String gender= result.getString("gender");
			String phone = result.getString("phone");
			String email = result.getString("email");
			String experience = result.getString("experience");
			String qualification =result.getString("qualification");
			String specialization = result.getString("specialization");
			String institute= result.getString("institute");
			String pyear = result.getString("pyear");
			String skills= result.getString("skills");
	           resumeBean2 = new ResumeBean( firstname, lastname, location, gender, phone, qualification, institute, pyear, skills, email, experience, specialization);
	           
	           conCollection.add(resumeBean2);
		}
		System.out.println(conCollection.size());
		return conCollection;
	}
	
	public List<ResumeBean> listAllResume(String email1) throws SQLException,ClassNotFoundException{
		List<ResumeBean> conCollection = new ArrayList<ResumeBean>();
	
		String query =  "SELECT * from resume";
		ResultSet result = st.executeQuery(query);
		ResumeBean resumeBean2;
		while(result.next()){
	           //Retrieve by column name
	          
			String firstname = result.getString("firstname");
			String lastname = result.getString("lastname");
			String location= result.getString("location");
			String gender= result.getString("gender");
			String phone = result.getString("phone");
			String qualification =result.getString("qualification");
			String institute= result.getString("institute");
			String pyear = result.getString("pyear");
			String skills= result.getString("skills");
			String email = result.getString("email");
			String experience = result.getString("experience");
			String specialization = result.getString("specialization");



	         resumeBean2 = new ResumeBean(firstname, lastname, location, gender, phone, qualification, institute, pyear, skills, email, experience, specialization);
	           
	           conCollection.add(resumeBean2);
		}
		System.out.println(conCollection.size());
		return conCollection;
	}
	
	
	

	
	public int updateResume(ResumeBean rBean) throws SQLException{	
		
		 int result;
			String firstname = rBean.getFirstname();
			String lastname = rBean.getLastname();
			String location= rBean.getLocation();
			String gender= rBean.getGender();
			String phone = rBean.getPhone();
			String email = rBean.getEmail();
			String experience = rBean.getExperience();
			String qualification =rBean.getQualification();
			String specialization = rBean.getSpecialization();
			String institute= rBean.getInstitute();
			String pyear = rBean.getPyear();
			String skills= rBean.getSkills();

		String query = "UPDATE resume set firstname='"+firstname+"',lastname='"+lastname+"',location='"+location+"',gender='"+gender+"',phone='"+phone+"',experience='"+experience+"',qualification='"+qualification+"',specialization='"+specialization+"',institute='"+institute+"',pyear='"+pyear+"',skills='"+skills+"' where email='"+email+"'";		       	
           result = st.executeUpdate(query);
           if(result>=1){
               System.out.println("Resume updated");
           }else{
               System.out.println("Resume not updated");
               conn.close();
               st.close();
               result = 0;
           }
           conn.close();
           st.close();
           return result;	   	       	    						
}

}