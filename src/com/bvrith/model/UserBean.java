package com.bvrith.model;

public class UserBean {
	private String email;
    private String password;
    private String name;
    private String gender;
    private String location;
    private String phone;
    private String workexp;
    private String skills;
    private String edu;
    private String image;
    
    
    public UserBean() {
		super();
	}
	public UserBean(String email, String password,
			String name, String gender, String location, String phone,
			String workexp,  String skills,String edu,String image) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.gender = gender;
		this.location = location;
		this.phone = phone;
		this.workexp = workexp;
	this.skills = skills;
		this.edu = edu;
		this.image= image;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWorkexp() {
		return workexp;
	}
	public void setWorkexp(String workexp) {
		this.workexp = workexp;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getEdu() {
		return edu;
	}
	public void setEdu(String edu) {
		this.edu = edu;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
}

