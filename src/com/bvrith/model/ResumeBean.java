package com.bvrith.model;

public class ResumeBean {
	private String firstname;
    private String lastname ;
    private String location ;
    private String gender;
    private String phone ;
    private String qualification ;
    private String institute;
    private String  pyear  ;
    private String  skills;
	private String  email ;
    private String  experience;
    private String  specialization;
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getInstitute() {
		return institute;
	}
	public void setInstitute(String institute) {
		this.institute = institute;
	}
	public String getPyear() {
		return pyear;
	}
	public void setPyear(String pyear) {
		this.pyear = pyear;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	public ResumeBean(String firstname, String lastname, String location,
			String gender, String phone, String qualification,
			String institute, String pyear, String skills, String email,
			String experience, String specialization) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.location = location;
		this.gender = gender;
		this.phone = phone;
		this.qualification = qualification;
		this.institute = institute;
		this.pyear = pyear;
		this.skills = skills;
		this.email = email;
		this.experience = experience;
		this.specialization = specialization;
	}
}