package com.bvrith.model;

public class PostaddBean {
	String id;
	String jobtitle;
	String vacancies;
	String jobdescription;
	String workexperience;
	String salarymin;
	String salarymax;
	String location;
	String industry;
	String area;
	String qualification;
	String companyname;
	String contactperson;
	
	String contactno;
	String email;
	String status;

	
	public PostaddBean(String id,String jobtitle, String vacancies,
			String jobdescription, String workexperience, String salarymin,
			String salarymax, String location, String industry, String area,
			String qualification, String companyname, String contactperson,
			String contactno, String email, String status) {
		super();
		this.id = id;
		this.jobtitle = jobtitle;
		this.vacancies = vacancies;
		this.jobdescription = jobdescription;
		this.workexperience = workexperience;
		this.salarymin = salarymin;
		this.salarymax = salarymax;
		this.location = location;
		this.industry = industry;
		this.area = area;
		this.qualification = qualification;
		this.companyname = companyname;
		this.contactperson = contactperson;
		this.contactno = contactno;
		this.email= email;
		this.status= status;

	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getVacancies() {
		return vacancies;
	}
	
	public void setVacancies(String vacancies) {
		this.vacancies = vacancies;
	}
	public String getJobdescription() {
		return jobdescription;
	}
	public void setJobdescription(String jobdescription) {
		this.jobdescription = jobdescription;
	}
	public String getWorkexperience() {
		return workexperience;
	}
	public void setWorkexperience(String workexperience) {
		this.workexperience = workexperience;
	}
	public String getSalarymin() {
		return salarymin;
	}
	public void setSalarymin(String salarymin) {
		this.salarymin = salarymin;
	}
	public String getSalarymax() {
		return salarymax;
	}
	public void setSalarymax(String salarymax) {
		this.salarymax = salarymax;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public String getContactperson() {
		return contactperson;
	}
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}	
	public String getEmail() {
		return email;
	}
	
	public String setEmail() {
		return email;
	}

}
