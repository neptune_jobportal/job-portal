package com.bvrith.model;

public class E_UserBean {

	private String email;
    private String password;
    private String confpassworrd;
    private String companyname;
	private String industry;
    private String nation;
    private String location;
    private String phone;
    public E_UserBean(String email, String password, String confpassworrd,
			String companyname, String industry, String nation,
			String location, String phone) {
		super();
		this.email = email;
		this.password = password;
		this.confpassworrd = confpassworrd;
		this.companyname = companyname;
		this.industry = industry;
		this.nation = nation;
		this.location = location;
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfpassworrd() {
		return confpassworrd;
	}
	public void setConfpassworrd(String confpassworrd) {
		this.confpassworrd = confpassworrd;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

}
