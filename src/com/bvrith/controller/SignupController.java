package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
//import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



//import javax.servlet.http.HttpSession;
import com.bvrit.DAO.UserDAO;
import com.bvrith.model.UserBean;


public class SignupController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		int result;
		String email =request.getParameter("email");
		String password = request.getParameter("password");
		String name =request.getParameter("name");
		String gender =request.getParameter("gender");
		String location = "";
	    location =request.getParameter("countries");
		System.out.println("location" + location);
	    String phone =request.getParameter("phone");
		String workexp =request.getParameter("workexp");
		String keyskills = request.getParameter("skills");
		String edu = request.getParameter("edu");
		String image = request.getParameter("image");

		if(email.equals("") || password.equals("")){
			response.sendRedirect("signup.jsp?msg=pleasefill the form");
		}
		else{
		try {
			UserDAO ud = new UserDAO();
			UserBean uBean = new UserBean(email, password, name, gender, location, phone, workexp, keyskills, edu, image);
			result = ud.createUser(uBean);
			System.out.println(result);
			if(result >= 1){
				response.sendRedirect("profilepic.jsp?msg=Sucessfully registered!!");
			}
			else if(result <1 ){
				response.sendRedirect("signup.jsp?msg="+email +" already exists! signup with different name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	}
}