package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.DAO.UserDAO;
import com.bvrith.model.E_UserBean;


public class E_signupController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		int result;
		String email =request.getParameter("email");
		String password = request.getParameter("password");
		String confpassworrd=request.getParameter("confpassworrd");
		String companyname =request.getParameter("companyname");
		String industry =request.getParameter("industry");
		String nation =request.getParameter("nation");
		String location =request.getParameter("location");
	    String phone =request.getParameter("phone");
		if(email.equals("") || password.equals("")){
			response.sendRedirect("E_signup.jsp?msg=pleasefill the form");
		}
		else{
		try {
			UserDAO ud = new UserDAO();
			E_UserBean euBean = new E_UserBean(email, password, confpassworrd,companyname, industry,nation, location, phone);
			result = ud.createE_User(euBean);
			System.out.println(result);

			if(result == 1){
				response.sendRedirect("E_Welcome?msg=Sucessfully registered!!");
			}
			else if(result != 1){
				response.sendRedirect("error.jsp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	}
}
