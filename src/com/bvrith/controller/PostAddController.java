package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.DAO.AdDAO;
import com.bvrith.model.PostaddBean;


public class PostAddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			try {
				doProcess(request, response);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			try {
				doProcess(request, response);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		int result=0;
		HttpSession hs = request.getSession();
		String email = (String) hs.getAttribute("sname");
		String id= request.getParameter("id");
		String jobtitle= request.getParameter("jobtitle");
		String vacancies = request.getParameter("vacancies");
		String jobdescription = request.getParameter("jobdescription");
		String workexperience = request.getParameter("workexperience");
		String salarymin = request.getParameter("salarymin");
		String salarymax = request.getParameter("salarymax");
		String location = request.getParameter("location");
		String industry = request.getParameter("industry");
		String area= request.getParameter("area");
		String qualification = request.getParameter("qualification");
		String companyname = request.getParameter("companyname");
		String contactperson = request.getParameter("contactperson");
		String contactno = request.getParameter("contactno");
		String status= request.getParameter("status");
	
		System.out.println("before try");
		try {
			AdDAO pdao = new AdDAO();
			PostaddBean pBean = new PostaddBean(id,jobtitle, vacancies, jobdescription, workexperience, salarymin, salarymax, location, industry, area, qualification, companyname, contactperson, contactno, email, status);
			 result = pdao.postadd(pBean);
			 System.out.println("result");
			 
			 if(result == 1){
					response.sendRedirect("E_viewads.jsp?msg=Vacancy sucessfully posted");
				}		
				else{
				response.sendRedirect("Postadd.jsp?msg= These user details already exists!");
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
}

