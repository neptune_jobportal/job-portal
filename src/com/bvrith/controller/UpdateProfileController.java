package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.DAO.UserDAO;
import com.bvrith.model.UserBean;


public class UpdateProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				doProcess(request, response);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try {
				doProcess(request, response);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		int result = 0;
		
		String email1 = request.getParameter("email1");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String gender= request.getParameter("gender");
		String location = request.getParameter("location");
		String phone = request.getParameter("phone");
		String workexp = request.getParameter("workexp");
		String skills = request.getParameter("skills");
		String edu = request.getParameter("edu");
		String image = request.getParameter("image");
		UserDAO user1;
		try {
			user1 = new UserDAO();
			UserBean uBean= new UserBean(email1, password, name, gender, location, phone, workexp, skills, edu, image);
			result = user1.updateProfile(uBean);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	if(result == 1){
				response.sendRedirect("ViewProfile.jsp?msg=Profile Sucessfully updated!!");
			}
			else{
				response.sendRedirect("UpdateProfile.jsp?msg=failed to update!! Try again...");
			}
		} 
		
		}
	

