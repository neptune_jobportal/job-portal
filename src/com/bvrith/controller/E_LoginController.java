package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.DAO.UserDAO;
import com.bvrit.log4j.Address;
import com.bvrith.model.E_UserBean;


public class E_LoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		boolean result;
		String email =request.getParameter("email");
		String password = request.getParameter("password");
		String confpassworrd=request.getParameter("confpassworrd");
		String companyname =request.getParameter("companyname");
		String industry =request.getParameter("industry");
		String nation =request.getParameter("nation");
		String location =request.getParameter("location");
	    String phone =request.getParameter("phone");
	    HttpSession hs = request.getSession();
		hs.setAttribute("sname", email);
		Address log = new Address();
		log.write(email);
		if(email.equals("") || password.equals("")){
			response.sendRedirect("signup.jsp?msg=pleasefill the form");
		}
		else{
		try {
			UserDAO ud = new UserDAO();
			E_UserBean euBean = new E_UserBean(email, password, confpassworrd,companyname, industry,nation, location, phone);
			result = ud.validateE_User(euBean);
			 if(result){
				 System.out.println("i am logged in");
				 response.sendRedirect("E_Welcome");
					
				}
				else{
					response.sendRedirect("E_login.jsp");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
