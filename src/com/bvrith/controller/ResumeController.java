package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.DAO.AdDAO;
import com.bvrit.DAO.ResumeDAO;
import com.bvrit.DAO.UserDAO;
import com.bvrith.model.PostaddBean;
import com.bvrith.model.ResumeBean;

public class ResumeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public ResumeController() {

    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			try {
				doProcess(request, response);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			try {
				doProcess(request, response);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		
		HttpSession hs = request.getSession();
		String email = (String) hs.getAttribute("sname");
		int result = 0;
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String location= request.getParameter("location");
		String gender= request.getParameter("gender");
		String phone = request.getParameter("phone");
		String email1 = request.getParameter("email");
		String experience = request.getParameter("experience");
		String qualification = request.getParameter("qualification");
		String specialization = request.getParameter("specialization");
		String institute= request.getParameter("institute");
		String pyear = request.getParameter("pyear");
		String skills= request.getParameter("skills");
		System.out.println("Inresumecontroller");
	
		
		try {
			ResumeDAO rd = new  ResumeDAO();
			System.out.println("in try");
			System.out.println(email1);
			ResumeBean rBean = new ResumeBean(firstname, lastname, location, gender, phone, qualification, institute, pyear, skills, email1, experience, specialization);
			System.out.println("after object");
			result = rd.createResume(rBean);
			System.out.println("result");

						 

			 if(result == 1){
					response.sendRedirect("viewResume.jsp?msg=Vacancy sucessfully posted");
				}		
				else{
				response.sendRedirect("resume.jsp?msg= These user details already exists!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
}