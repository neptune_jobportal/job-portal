
package com.bvrith.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.DAO.UserDAO;
import com.bvrit.log4j.Address;
import com.bvrith.model.UserBean;

public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, NamingException {
		boolean result;
		System.out.println("hello");
		
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confpassworrd = request.getParameter("confpassworrd");
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String location = request.getParameter("location");
		String phone = request.getParameter("phone");
		String workexp = request.getParameter("workexp");
		String skills = request.getParameter("skills");
		String edu = request.getParameter("edu");
		String image = request.getParameter("image");
		HttpSession hs = request.getSession();
		hs.setAttribute("sname", email);
	//	System.out.println(sname);
		Address log = new Address();
		log.write(email);
		UserDAO ud;
		try {
			ud = new UserDAO();
			UserBean uBean = new UserBean(email, password, name, gender, location, phone, workexp, skills, edu, image);
			result = ud.validateUser(uBean);
    		if(result){
    			response.sendRedirect("WelcomePage.view");
			}
			else{
				response.sendRedirect("loginpage.jsp");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

