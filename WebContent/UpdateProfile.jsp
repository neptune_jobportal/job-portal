<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import = "com.bvrit.DAO.*" %>
<%@page import = "com.bvrith.model.*" %>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<title>Naukri Adda.com</title>
</head>
<body>
<jsp:include page="links.html" >
    <jsp:param name="param1" value="menu" />
</jsp:include>
<%
HttpSession hs = request.getSession();
String email1 = (String)hs.getAttribute("sname");

String email = request.getParameter("email");
String password = request.getParameter("password");
String name = request.getParameter("name");
String gender= request.getParameter("gender");
String location = request.getParameter("location");
String phone = request.getParameter("phone");
String workexp = request.getParameter("workexp");
String skills = request.getParameter("skills");
String edu = request.getParameter("edu");
String resume = request.getParameter("resume");
String image = request.getParameter("image");


UserBean ub = new UserBean(email,password,name,gender,location,phone,workexp,skills,edu,image);
 ub.setEmail(email);
%>

<div class = 'well-lg' >
<div class="container" style="margin: 0px 0px 62px 0px">
<div class="col-sm-4"></div>
<div class="col-sm-4" >
<form action= "UpdateProfileController">
<table align="center" class="table table-striped">
<thead>

<tr><th colspan="3" align="center"><h2>Update Profile</h2></th></tr></thead>

<tr><td>Email:</td><td><input type = "text" name = id id="disabledTextInput" value = "<%=email%>" readonly="readonly"></td></tr>
<tr><td>Name:</td><td><input type = text name = name value = "<%=name%>"></td></tr>
<tr><td>Gender:</td><td><input type = text name =gender value ="<%=gender%>" ></td></tr>
<tr><td>Location:</td><td><input type = text name = location value ="<%=location%>" ></td></tr>
<tr><td>Phone number:</td><td><input type = text name = phone value = "<%=phone%>"></td></tr>
<tr><td>Work Experience:</td><td><input type = text name = workexp value ="<%=workexp%>" ></td></tr>
<tr><td>Skills:</td><td><input type = text name = skills value ="<%=skills%>" ></td></tr>
<tr><td>Education:</td><td><input type = text name = edu value ="<%=edu%>" ></td></tr>


<tr><td colspan="2" align="center"><input type = submit value = Update ></td></tr>
</table>
</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>" +  msg + "</font color></td></tr></table>");
}
%>
</div>
</div>
</div>
</body>
</html>