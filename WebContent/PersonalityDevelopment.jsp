<!DOCTYPE html>
<html lang="en">
<head>
  <title>Naukri Adda.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
    <h1><font color = "White"> Personality Development</font></h1>
    </div>
    <div>
      <ul class="nav navbar-nav">
       
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./images/pd1.jpg" alt="Chania" width="460" height="345">
      </div>

       <div class="item">
        <img src="./images/pd2.jpg" alt="Flower" width="460" height="345">
       </div>
       <div class="item">
        <img src="./images/pd3.jpg" alt="Flower" width="460" height="345">
       </div>
      
     
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <p>&nbsp</p>
   <p>&nbsp</p>
  <p><h3><b>Personality Development.</h3></b></p>
  <p><h4> How often do we hear this term, from our mentors, our teachers, on the covers of self-help books or on the banners of institutes and learning centres? The abundance of the usage of this term signals towards its importance in today’s life. So what do you mean by personality development? Is it something about how you look, or how you speak? Or is it how easily you can connect with people? Personality development is none of these. Or somewhere it is all of it. In order to survive in today’s world one needs to be smart and quick-witted all the time. It’s no longer just about how much effort you put into your work but one’s personality also has a lot to do with what one achieves. Here I have got 12 simple yet crucial tips over how you can acquire a well-meaning personality.</h4></p>
  <p><h4><b>Know yourself</b></h4></p>
  <p><h4>Obviously before you get on developing something you need to know all about it first. The same goes with your personality. One needs to start with taking a good look at themselves, analyzing their traits, the strengths and weaknesses and everything that needs to be worked upon. Don’t shy away from accepting your flaws and learn about yourself as much as you can.</h4></p>
  <p><h4><b>Bring positivity in your outlook</b></h4></p>
  <p><h4>Your thoughts and your actions both need to be positive in order to have an attractive personality. The way we think has a lot of effect on the way how we act. And if one prospers positive thoughts inside his mind then that also gives him a confidence boost and enhances their personality. Situations and circumstances in life can always be full highs and lows. But in order to adopt a positive outlook towards life, you need to find the brighter side of the things and focus on the good parts.</h4></p>
  <p><h4><b>Have an opinion</b></h4></p>
  <p><h4>Having an opinion and being able to confidently put it forward doesn’t just help making your conversations interesting but it also makes you look more influential and well informed around other people. Never shy away from projecting your opinions even if they happen to conflict with those of other people. Be well informed about all the relevant stuff in your surrounding and fell free to have opinions. It will make yourself feel important too</h4></p>
  <p><h4><b>Meet new people</b></h4></p>
  <p><h4>Meeting new and different kinds of people is a healthy step towards expanding your horizons and exposing yourself to a larger number of things. You get an opportunity to know more about other cultures and lifestyles and it significantly has a positive effect on your own personality.</h4></p>
  <p><h4><b>Read more often and develop new interests</b></h4></p>
  <p><h4>A man of very few interests has very little to talk about. But if you are well informed about things and cultivate a number of interests, more people tend to like you. You can strike up interesting conversations instead of appearing to be dull and monotonous. When you meet new people you do not have to think about what to say as you can share your knowledge or your interests and get them indulged in conversation.</h4></p>
  <p><h4><b>Be a good listener</b></h4></p>
  <p><h4>“Most people do not listen with the intent to understand; they listen with the intent to reply.”  True it is. Being a good listener may not seem like but it is an important step towards achieving a more likable personality. When somebody talks to you, listen with interest and give them all the attention and importance. Maintain a direct eye contact and do not get distracted by the surroundings. It will help you in knowing a better deal about people and attending them in a better way.</h4></p>
  <p><h4><b> Be a little fun</b></h4></p>
  <p><h4>Oh yes, this is necessary! Being able to find a humorous side in otherwise terrible situations and bringing a little quirkiness of your own is appreciated by one and all. Everybody loves a person who can make them laugh and bring a funny perspective to the regular things of life. One does not need to be all serious and sober all the time but adorning your funny hat (not literally) every once in a while will turn you in to a more charming personality.</h4></p>
  <p><h4><b>Be courteous</b></h4></p>
  <p><h4>Being courteous is never out of fashion and is well appreciated and respected by everyone. Be humble and greet everyone with a smile. Never shy away from helping or supporting your peers and being available to them whenever they need you. Doing random acts of kindness will not just make somebody else’s day but it will also make you come along as a pleasing person. Also it will give your personality a confidence boost. Be humble and down to earth to your juniors and seniors alike.</h4></p>
  <p><h4><b>Work on your Body Language</b></h4></p>
  <p><h4>Body language is just as important for your personality as your verbal communication skills. It tells a lot about yourself and helps people in making accurate conjectures about you. Everything including the way you walk, sit, talk or eat leaves an impact over the people around you and having a correct body language can do wonders for your personality. Walk in an upright position with shoulders straight. Do not droop. Sit in a relaxed posture and make always eye contact while speaking.</h4></p>
  <p><h4><b>Check your attire</b></h4></p>
  <p><h4>I am not abruptly beginning to emphasize about your exterior self instead of your skills and abilities but one’s attire has an important role to play while making a desirable impression. And not just that, but it also gives yourself a confidence boost knowing that you look good and are dressed appropriately. Dress up in a decent manner and keeping your surroundings in mind. While flashy colors and too much body tattoos or piercings convey an unprofessional attitude, neatly ironed clothes make you look presentable.</h4></p>
  <p><h4><b>Be yourself</b></h4></p>
  <p><h4>Though one can always look up to other people to take an inspiration from, but you should still remain your own unique self. Each one of us is different, we have our own sets of skills and flaws and trying to be somebody else gets you nowhere and just simply backfires. Trying too hard to fit in a new group or wanting to belong should never take your authenticity and singularity away. Never try moulding into another person but instead work on being the best version of yourself.</h4></p>
  <p><h4><b>Be confident</b></h4></p>
  <p><h4>Yes, that’s the key. Being confident about who you are and what you are doing is the most important tip for personality development. Never doubt your capabilities and if there is something you need to work upon then put in all the effort so you can come over your fears and gain confidence. Read success stories or surround yourself with motivational thoughts or “encouragements” which can boost up your self esteem and help you in attaining a charming personality. Just everything you do, have faith in yourself and put in your hard work. There can be nothing more appealing in your personality than an incredible confidence.</h4></p>
 </div>

</body>
</html>