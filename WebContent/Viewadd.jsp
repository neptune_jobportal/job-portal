 <%-- <%@page import="java.io.PrintWriter"%> --%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@page import = "com.bvrith.model.*"%>
<%@page import = "com.bvrit.DAO.*" %>



<%@ page import="java.util.ListIterator"%>
<%@ page import="java.util.*"%>

<%@ page import="javax.servlet.ServletException"
import="javax.servlet.http.HttpServlet"
import="javax.servlet.http.HttpServletRequest"
import="javax.servlet.http.HttpServletResponse"
%>
  
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
     <%   
    HttpSession hs = request.getSession();
     String number = (String)hs.getAttribute("number");
  
	 int recordsPerPage = 6;
	 int pages = 1;
	 if(request.getParameter("pages") != null)
	   pages = Integer.parseInt(request.getParameter("pages"));
	 
    
    AdDAO qdao = new AdDAO();
    List<PostaddBean> list = qdao.listadd(((pages-1) * recordsPerPage),recordsPerPage);
    ListIterator<PostaddBean> lt = list.listIterator();
    //response.setContentType("text/html");
    
     int noOfRecords = qdao.getNoOfRecords();
	 int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
	out.print(noOfPages);
	out.print(noOfRecords);
	 request.setAttribute("noOfPages", noOfPages);
	 request.setAttribute("currentPage", pages);
    
    %>   
 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <%--  <%
	AdDAO pdao = new AdDAO();
List<PostaddBean> lst = pdao.listadd();
ListIterator<PostaddBean> lt = lst.listIterator();
response.setContentType("text/html");
%>   --%>
<html lang="en">
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<title>Naukri Adda.com</title> 
</head>
<head>
 <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 <link rel="stylesheet" 
href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" 
src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
 
<script>
$(document).ready(function(){
	
    $('#keyword').dataTable();
});
</script>
</head>
<body>
   
<jsp:include page="links.html" >
    <jsp:param name="param1" value="menu" />
</jsp:include>   
<div class = 'well-lg'></div> 

<body> 
<%-- <div class="table-responsive"><!-- container -->

 <%String msg = request.getParameter("msg");
if(msg != null){
	out.print("<p align=center><font color=blue size = 4> "+  msg + "</font></p>");
}%>  --%>

  
 <div class ="table-responsive">
<table class = "sortable-bootstrap"  id ="keyword" width="100%">
<!-- <tr><th align="center"><h2>Vacancies</h2></th></tr>  -->
<!-- <table class="table table-bordered"> -->
<thead>
      <tr>
        <th>Title</th>
        <th>No.of vacancies</th>
        <th>Job Description</th>
        <th>Work experience</th>
        <th>Salary range</th>
        <th>Location</th>
        <th>Industry</th>
        <th>Area</th>
        <th>Qualification</th>
        <th>Company name</th>
        <th>Contact person</th>
        <th>Contact no</th>
        <th>Status</th>
      </tr>
    </thead>
<tbody>

<%while(lt.hasNext()){
     PostaddBean pb = lt.next();
     out.print("<tr>");
     out.print("<td>"+pb.getJobtitle()+"</td>");
     out.print("<td>"+pb.getVacancies()+"</td>");
     out.print("<td>"+pb.getJobdescription()+"</td>");
     out.print("<td>"+pb.getWorkexperience()+"</td>");
     out.print("<td>"+pb.getSalarymin()+" to "+pb.getSalarymax()+"</td>");
     out.print("<td>"+pb.getLocation()+"</td>");
     out.print("<td>"+pb.getIndustry()+"</td>");
     out.print("<td>"+pb.getArea()+"</td>");
     out.print("<td>"+pb.getQualification()+"</td>");
     out.print("<td>"+pb.getCompanyname()+"</td>");
     out.print("<td>"+pb.getContactperson()+"</td>");
     out.print("<td>"+pb.getContactno()+"</td>");
     out.print("<td>"+pb.getStatus()+"</td>)");
     out.print("</tr>");

}
%>
</tbody>
</table>
</div>
 <%--For displaying Previous link except for the 1st page --%>
	<c:if test="${currentPage != 1}">
		<td><a href="Viewadd.jsp?pages=${currentPage - 1}">Previous</a></td>
	</c:if>
	
	<%--For displaying Page numbers. 
	The when condition does not display a link for the current page--%>
	<table border="1" cellpadding="5" cellspacing="5" class = "table table-hover table-striped table-bordered" style="color : black; fontsize:40px;">
		<tr>
			<c:forEach begin="1" end="${noOfPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<td>${i}</td>
					</c:when>
					<c:otherwise>
						<td><a href="Viewadd.jsp?pages=${i}">${i}</a></td>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tr>
	</table>
	
	<%--For displaying Next link --%>
	<c:if test="${currentPage lt noOfPages}">
		<td><a href="Viewadd.jsp?pages=${currentPage + 1}">Next</a></td>
	</c:if>
 
</body>
</html>