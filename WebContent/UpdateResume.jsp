<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import = "com.bvrit.DAO.*" %>
<%@page import = "com.bvrith.model.*" %>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<title>Naukri Adda.com</title>
</head>
<body>
<jsp:include page="links.html" >
    <jsp:param name="param1" value="menu" />
</jsp:include>
<%
HttpSession hs = request.getSession();
String email1 = (String)hs.getAttribute("sname");

String firstname = request.getParameter("firstname");
String lastname = request.getParameter("lastname");
String location= request.getParameter("location");
String gender= request.getParameter("gender");
String phone = request.getParameter("phone");
String email = request.getParameter("email");
String experience = request.getParameter("experience");
String qualification = request.getParameter("qualification");
String specialization = request.getParameter("specialization");
String institute= request.getParameter("institute");
String pyear = request.getParameter("pyear");
String skills= request.getParameter("skills");
ResumeBean ub = new ResumeBean(firstname,lastname,location,gender,phone,email,experience,qualification,specialization,institute,pyear,skills);
 ub.setEmail(email);
%>

<div class = 'well-lg' >
<div class="container" style="margin: 0px 0px 62px 0px">
<div class="col-sm-4"></div>
<div class="col-sm-4" >
<form action= "UpdateResumeController">
<table align="center" class="table table-striped">
<thead>

<tr><th colspan="3" align="center"><h2>Update Resume</h2></th></tr></thead>

<tr><td>FirstName:</td><td><input type = "text" name = firstname id="disabledTextInput" value = "<%=firstname%>" readonly="readonly"></td></tr>
<tr><td>LastName:</td><td><input type = text name = lastname value = "<%=lastname%>"></td></tr>
<tr><td>Location:</td><td><input type = text name = location value ="<%=location%>" ></td></tr>
<tr><td>Gender:</td><td><input type = text name =gender value ="<%=gender%>" ></td></tr>
<tr><td>Phone number:</td><td><input type = text name = phone value = "<%=phone%>"></td></tr>
<tr><td>Email:</td><td><input type = text name = email value ="<%=email%>" ></td></tr>
<tr><td>Work Experience:</td><td><input type = text name = experience value ="<%=experience%>" ></td></tr>
<tr><td>Qualification:</td><td><input type = text name = qualification value ="<%=qualification%>" ></td></tr>
<tr><td>Specialization:</td><td><input type = text name = specialization value ="<%=specialization%>" ></td></tr>
<tr><td>Institute:</td><td><input type = text name = institute value ="<%=institute%>" ></td></tr>
<tr><td>Year of passout:</td><td><input type = text name = pyear value ="<%=pyear%>" ></td></tr>
<tr><td>Skills:</td><td><input type = text name = skills value ="<%=skills%>" ></td></tr>


<tr><td colspan="2" align="center"><input type = submit value = Update ></td></tr>
</table>
</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>" +  msg + "</font color></td></tr></table>");
}
%>
</div>
</div>
</div>
</body>
</html>