<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Naukri Adda.com</title>

    <!-- Bootstrap Core CSS -->v
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	
	<div class="brand"><font color='black'> NaukriAdda .com</div>
    <div class="address-bar">A place to get placed</font></div>
	<form action = "SignupController">
   <div class="row">
            <div class="box">
            
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>SignUp here!!</strong>
                    </h2>
                    <hr class="visible-xs">
                     <hr>
                <div class="col-md-4"></div>
			<div class="form-group col-lg-4">
				<table align="center" class="table">
		
				<thead>
				<tr><th colspan="2" align="center"><h3>Create your account</h3></th></tr></thead>
				<tr><td><b>Email:</b></td><td><input type+ = "email" placeholder = "Enter email" name = email class="form-control"required></td></tr>
				<tr><td><b>Password:</b></td><td><input type = password placeholder = "Password" name = password class="form-control"required></td></tr>
				<tr><td><b>Name:</b></td><td><input type = text placeholder = "Name" name = name class="form-control"required></td></tr>
				<tr><td><b>Gender:</b></td>
					<td><input type =radio name = gender value = Male required>&nbspMale
					<input type =radio name = gender value = Female required>&nbspFemale</td>
				 <tr><td><b>Location:</b></td><td>
	
  <select   name="location" class="form-control" > 

    <option value="Ahmedabad">Ahmedabad</option>
    <option value="Alwar">Alwar</option>
    <option value="Alappuzha">Alappuzha</option>
    <option value="Amritsar">Amritsar</option>
    <option value="Aurangabad">Aurangabad</option>
    <option value="Banglore">Banglore</option>
    <option value="Bharatpur">Bharatpur</option>
    <option value="Bhikanar">Bhikanar</option>
    <option value="Bhopal">Bhopal</option>
    <option value="Bhubaneshwar">Bhubaneshwar</option>
    <option value="Bodhgaya">Bodhgaya</option>
    <option value="Chandigarh">Chandigarh</option>
    <option value="Chennai">Chennai</option>
    <option value="Coimbatore">Coimbatore</option>
    <option value="Cuttack">Cuttack</option>
    <option value="Dalhousie">Dalhousie</option>
    <option value="Delhi">Delhi</option>
    <option value="Diu-island">Diu-island</option>
    <option value="Ernakulam">Ernakulam</option>
    <option value="Faridabad">Faridabad</option>
    <option value="Gangtok">Gangtok</option>
    <option value="Ghaziabad">Ghaziabad</option>
    <option value="Gurgaon">Gurgaon</option>
    <option value="Guwahati">Guwahati</option>
    <option value="Gwalior">Gwalior</option>
    <option value="Haridwar">Haridwar</option>
    <option value="Hyderabad">Hyderabad</option>
    <option value="Imphal">Imphal</option>
    <option value="Indore">Indore</option>
    <option value="Jabalpur">Jabalpur</option>
    <option value="Jaipur">Jaipur</option>
    <option value="Jaisalmer">Jaisalmer</option>
    <option value="Jalandhar">Jalandhar</option>
    <option value="Jamshedpur">Jamshedpur</option>
    <option value="Jodhpur">Jodhpur</option>
    <option value="Kanpur">Kanpur</option>
    <option value="Kanyakumari">Kanyakumari</option>
    <option value="Kochi">Kochi</option>
    <option value="Kodiakanal">Kodiakanal</option>
    <option value="Kolkata">Kolkata</option>
    <option value="Kota">Kota</option>
    <option value="Kottayam">Kottayam</option>
    <option value="Lucknow">Lucknow</option>
    <option value="Ludhiana">Ludhiana</option>
    <option value="Madurai">Madurai</option>
    <option value="Manali">Manali</option>
    <option value="Mangalore">Mangalore</option>
    <option value="Mathura">Mathura</option>
    <option value="Mount-Abu">Mount-Abu</option>
    <option value="Mumbai">Mumbai</option>
    <option value="Mussoorie">Mussoorie</option>
    <option value="Mysore">Mysore</option>
    <option value="Nagpur">Nagpur</option>
    <option value="Noida">Noida</option>
    <option value="Ooty">Ooty</option>
    <option value="Panaji">Panaji</option>
    <option value="Pondicherry">Pondicherry</option>
    <option value="Porbandar">Porbandar</option>
    <option value="Portblair">Portblair</option>
    <option value="Pune">Pune</option>
    <option value="Puri">Puri</option>
    <option value="Rajkot">Rajkot</option>
    <option value="Rameshwaram">Rameshwaram</option>
    <option value="Ranchi">Ranchi</option>
    <option value="Secunderabad">Secunderabad</option>
    <option value="Shimla">Shimla</option>
    <option value="Surat">Surat</option>
    <option value="Thanjavur">Thanjavur</option>
    <option value="Tiruchchirapalli">Tiruchchirapalli</option>
    <option value="Thissur">Thissur</option>
    <option value="Tirumala">Tirumala</option>
    <option value="Udaipur">Udaipur</option>
    <option value="Vadodra">Vadodra</option>
    <option value="Varanasi">Varanasi</option>
    <option value="Vijayawada">Vijayawada</option>
    <option value="Visakhapatnam">Visakhapatnam</option>
</select></td></tr>
				
				<tr><td><b>Phone:</b></td><td><input type = text name = phone placeholder = "Phone no" class="form-control"required></td></tr>
				<tr><td><b>Work experience:</b></td><td><input type = text name = workexp placeholder = "Work experience"class="form-control" required></td></tr>
				<tr><td><b>Key skills:</b></td><td><input type = text name = skills placeholder = "Skills" class="form-control" required></td></tr>
				<tr><td><b>Education:</b></td><td><input type = text name = edu placeholder = "Education" class="form-control" required></td></tr>
				
				</table>

    			
    			<tr><a href="profilepic.jsp">image</a></tr>
    			</div>
    			  </div>
            </div>
        </div>
    			
    	<!-- 		<div>
 Image<input type = "file" id="fileupload" name ="file" required/></td></tr>
<hr />
<b>Live Preview</b>
<br />
<br />
<div id="dvPreview">
</div>
</div> -->
    			
    			<center><input type = submit value = Register></center><a href="loginpage.jsp"></a>
    			
			</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>*" +  msg + "</font color></td></tr></table>");
}
%>
	
<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Naukri Adda.com 2015</p>
                </div>
            </div>
        </div>
   </footer>
 
</body>
</html>
		
















