<%@page import="com.bvrith.model.PostaddBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<title>Naukri Adda.com</title>
</head>
<body >
<jsp:include page="E_links.html" >
    <jsp:param name="param1" value="menu" />
</jsp:include>

<div class = 'well-lg'>

<div class="container" style="margin: -100px 0px 70px 0px">
<div class="col-sm-4"></div>


<div class="col-sm-4" style="margin-top: 100px">

<form action="PostAddController" >
<table align="center" class="table table-striped">
<thead>
<tr><th colspan="3" align="center"><h2>Post Advertisements</h2></th></tr></thead>
<tr><td>Job title:</td><td>
<!-- <input type="text" list="countries" /> -->
<!-- <datalist id="countries"> -->
 <select name = jobtitle required> 

	<option value="Executive">Executive</option>
	<option value="Senior executive">Senior executive</option>
	<option value="managing director">managing director</option>
	<option value="Vice president">Vice president</option>
	<option value="general manager">general manager</option>
	<option value="department head">department head</option>
	<option value="Deputy general manager">Deputy general manager</option>
	<option value="Manager">Manager</option>
	<option value="section head">section head</option>
	<option value="Assistant manager">Assistant manager</option>
	<option value="team leader">team leader</option>
	<option value="Staff">Staff</option>
	<option value="Project manager">Project manager</option>
	<option value="Trainer">Trainer</option>
</datalist>
</td></tr>
<tr><td>Number Of Vacancies:</td><td><input type = text name = vacancies required></td></tr>
<tr><td>Job Description:</td><td><input type = text name = jobdescription required></td></tr>
<tr><td>Work Experience:</td><td><input type = text name = workexperience required></td></tr>
<tr><td>salary:</td><td><input type = text name =salarymin>to<input type = text name = salarymax required></td></tr>

<tr><td>Location of Jobs:</td><td> 



<select name = location required> 
	<option value="Agra">Agra</option>
    <option value="Ahmedabad">Ahmedabad</option>
    <option value="Alwar">Alwar</option>
    <option value="Alappuzha">Alappuzha</option>
    <option value="Amritsar">Amritsar</option>
    <option value="Aurangabad">Aurangabad</option>
    <option value="Banglore">Banglore</option>
    <option value="Bharatpur">Bharatpur</option>
    <option value="Bhikanar">Bhikanar</option>
    <option value="Bhopal">Bhopal</option>
    <option value="Bhubaneshwar">Bhubaneshwar</option>
    <option value="Bodhgaya">Bodhgaya</option>
    <option value="Chandigarh">Chandigarh</option>
    <option value="Chennai">Chennai</option>
    <option value="Coimbatore">Coimbatore</option>
    <option value="Cuttack">Cuttack</option>
    <option value="Dalhousie">Dalhousie</option>
    <option value="Delhi">Delhi</option>
    <option value="Diu-island">Diu-island</option>
    <option value="Ernakulam">Ernakulam</option>
    <option value="Faridabad">Faridabad</option>
    <option value="Gangtok">Gangtok</option>
    <option value="Ghaziabad">Ghaziabad</option>
    <option value="Gurgaon">Gurgaon</option>
    <option value="Guwahati">Guwahati</option>
    <option value="Gwalior">Gwalior</option>
    <option value="Haridwar">Haridwar</option>
    <option value="Hyderabad">Hyderabad</option>
    <option value="Imphal">Imphal</option>
    <option value="Indore">Indore</option>
    <option value="Jabalpur">Jabalpur</option>
    <option value="Jaipur">Jaipur</option>
    <option value="Jaisalmer">Jaisalmer</option>
    <option value="Jalandhar">Jalandhar</option>
    <option value="Jamshedpur">Jamshedpur</option>
    <option value="Jodhpur">Jodhpur</option>
    <option value="Kanpur">Kanpur</option>
    <option value="Kanyakumari">Kanyakumari</option>
    <option value="Kochi">Kochi</option>
    <option value="Kodiakanal">Kodiakanal</option>
    <option value="Kolkata">Kolkata</option>
    <option value="Kota">Kota</option>
    <option value="Kottayam">Kottayam</option>
    <option value="Lucknow">Lucknow</option>
    <option value="Ludhiana">Ludhiana</option>
    <option value="Madurai">Madurai</option>
    <option value="Manali">Manali</option>
    <option value="Mangalore">Mangalore</option>
    <option value="Mathura">Mathura</option>
    <option value="Mount-Abu">Mount-Abu</option>
    <option value="Mumbai">Mumbai</option>
    <option value="Mussoorie">Mussoorie</option>
    <option value="Mysore">Mysore</option>
    <option value="Nagpur">Nagpur</option>
    <option value="Noida">Noida</option>
    <option value="Ooty">Ooty</option>
    <option value="Panaji">Panaji</option>
    <option value="Pondicherry">Pondicherry</option>
    <option value="Porbandar">Porbandar</option>
    <option value="Portblair">Portblair</option>
    <option value="Pune">Pune</option>
    <option value="Puri">Puri</option>
    <option value="Rajkot">Rajkot</option>
    <option value="Rameshwaram">Rameshwaram</option>
    <option value="Ranchi">Ranchi</option>
    <option value="Secunderabad">Secunderabad</option>
    <option value="Shimla">Shimla</option>
    <option value="Surat">Surat</option>
    <option value="Thanjavur">Thanjavur</option>
    <option value="Tiruchchirapalli">Tiruchchirapalli</option>
    <option value="Thissur">Thissur</option>
    <option value="Tirumala">Tirumala</option>
    <option value="Udaipur">Udaipur</option>
    <option value="Vadodra">Vadodra</option>
    <option value="Varanasi">Varanasi</option>
    <option value="Vijayawada">Vijayawada</option>
    <option value="Visakhapatnam">Visakhapatnam</option>	</select>
</td></tr>
<tr><td>Industry:</td><td>

<!-- <datalist id="industry">  -->
<select name = industry required> 
	<option value="Accounting">Accounting</option>
	<option value="Advertising">Advertising</option>
	<option value="Aerospace">Aerospace</option>
	<option value="Aircraft">Aircraft</option>
    <option value="Airline">Airline</option>
    <option value="Automotive">Automotive</option>
    <option value="Banking">Banking</option>
    <option value="Broadcasting">Broadcasting</option>
    <option value="Biotechnology">Biotechnology</option>
    <option value="Cargo Handling">Cargo Handling</option>
    <option value="Computer">Computer</option>
    <option value="Consulting">Consulting</option>
    <option value="Cosmetics">Cosmetics</option>
    <option value="Defense">Defense</option>
    <option value="Education">Education</option>
    <option value="Electronics">Electronics</option>
    <option value="Entertainment">Entertainment</option>
    <option value="Executive Search">Executive Search</option>
    <option value="Financial Services">Financial Services</option>
    <option value="Food, Beverage & Tobacco">Food, Beverage & Tobacco</option>
    <option value="Grocery">Grocery</option>
    <option value="Health Care">Health Care</option>
    <option value="Internet Publishing">Internet Publishing</option>
    <option value="Investment Banking">Investment Banking</option>
    <option value="Manufacturing">Manufacturing</option>
    <option value="Music">Music</option>
    <option value="Newspaper Publishers">Newspaper Publishers</option>
    <option value="Pharmaceuticals">Pharmaceuticals</option>
    <option value="Private Equity">Private Equity</option>
    <option value="Publishing">Publishing</option>
    <option value="Real Estate">Real Estate</option>
    <option value="Retail & Wholesale">Retail & Wholesale</option>
    <option value="Securities & Commodity Exchanges">Securities & Commodity Exchanges</option>
    <option value="Service">Service</option>
    <option value="Software">Software</option>
    <option value="Softwaret">Software</option>
    <option value="Technology">Technology</option>
    <option value="Telecommunications">Telecommunications</option>
    <option value="Transportation">Transportation</option>
    <option value="Venture Capital">Venture Capital</option>

</select></td></tr>

<tr><td>Functional area</td><td>


<select name = area required> 
	<option value="Sales_Department">Sales_Department</option>
	<option value="Accounting">Accounting</option>
	<option value="Marketing">Marketing</option>
	<option value="Finance">Finance</option>
	<option value="Human_Resource">Human_Resource</option>
	<option value="Production">Production</option>
</select>	</td></tr>
<tr><td>Qualification</td><td>

<!-- <datalist id="qualification"> -->
 <select name = qualification required> 
	<option value="B.Tech">B.Tech</option>
	<option value="M.Tech">M.Tech</option>
	<option value="MBA">MBA</option>
	<option value="M.Sc">M.Sc</option>
	<option value="B.Sc">B.Sc</option>
	<option value="B.Pharm">B.Pharm</option>
	<option value="M.pharm">M.pharm</option>
	<option value="SSC">SSC</option>
	<option value="Intermediate">Intermediate</option>
	<option value="Not_persuing_graduation">Not_persuing_graduation</option>
	<option value="Agra">Agra</option>
	</select></td></tr>
<tr><td>Company name:</td><td><input type = text name = companyname required></td></tr>
<tr><td>Contact Person:</td><td><input type = text name = contactperson required></td></tr>
<tr><td>Contact Number:</td><td><input type = text name = contactno required></td></tr>

<tr><td>Status</td><td>


<select name = status required> 
	<option value="Vacency open">Vacency open</option>
	<option value="Vacency closed">Vacency closed</option>
	</select>	</td></tr>
	

<tr><td colspan="6" align="center"><a href = "Viewadd.jsp"><input type = submit value = PostADD></a></td></tr>
<!-- td colspan="5" align="center"><a href="WelcomePage.view"><input type = button value = back></a></td> -->
</table>
</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>*" +  msg + "</font color></td></tr></table>");
}
%>
</div>
</div>
</div>
</body>
</html>