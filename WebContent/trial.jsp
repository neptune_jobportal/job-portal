<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Naukri Adda.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }
  </style>
</head>
<title>Naukri Adda.com</title>
<nav class="navbar navbar">
  <div class="container-fluid">
    <div class="navbar-header"></div>
    <div>
		<ul class="nav navbar-nav">
        	<li class="active"><a href="#"><img src = ./images/Logo.png width = 200px height = 200px></a></li>
            <li><a href="#">&nbsp&nbsp&nbsp Jobs &nbsp&nbsp&nbsp</a></li>
	        <li><a href="#">&nbsp&nbsp&nbspCompanies&nbsp&nbsp&nbsp</a></li>
    	    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">&nbsp&nbsp&nbspServices&nbsp&nbsp&nbsp<span class="caret"></span></a>
        	<ul class="dropdown-menu">
            	<li><a href="#">Resume writing</a></li>
            	<li><a href="#">Page 1-2</a></li>
            	<li><a href="#">Page 1-3</a></li>
          	</ul>
       	 	<li><a href="loginpage.jsp">&nbsp&nbsp&nbspEmployee Login&nbsp&nbsp&nbsp</a></li>
        	<li><a href="E_login.jsp">&nbsp&nbsp&nbspEmployer Login&nbsp&nbsp&nbsp</a></li>
		</ul>
    </div>
  </div>
</nav>
<body>
	<div class="container">
		<form action = "LoginController"></form>
	  <br>
 	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
 	  
 	  	<!-- Indicators -->
    	<ol class="carousel-indicators">
     	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      	<li data-target="#myCarousel" data-slide-to="1"></li>
      	<li data-target="#myCarousel" data-slide-to="2"></li>
      	<li data-target="#myCarousel" data-slide-to="3"></li>
    	</ol>
		
        <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">
    		<div class="item active">
        		<img src="./images/1.png" alt="Chania" width="460" height="345">
      		</div>

      		<div class="item">
        		<img src="./images/2.png" alt="Chania" width="460" height="345">
      		</div>
   
      		<div class="item">
        		<img src="./images/3.png" alt="Flower" width="460" height="345">
      		</div>
     
    		<!-- Left and right controls -->
	    	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      			<span class="sr-only">Previous</span>
    		</a>
    		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      			<span class="sr-only">Next</span>
    		</a>
  		</div>
	</div>
	</div>
  </body>
</html>