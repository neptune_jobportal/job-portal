<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
    <img src = "./images/nlogo.png">
   
    </div>
    <div>
      <ul class="nav navbar-nav">
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./images/n1.jpg" alt="Chania" width="460" height="345">
      </div>

      <div class="item">
        <img src="./images/n2.jpg" alt="Chania" width="460" height="345">
      </div>
   
      <div class="item">
        <img src="./images/n3.jpg" alt="Flower" width="460" height="345">
      </div>

      <div class="item">
        <img src="./images/n4.jpg" alt="Flower" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<p><h3><PRE>Novartis is a global healthcare company based in Switzerland that provides solutions to the evolving needs of patients worldwide.</h3></p>

<p><h4>Novartis (NYSE: NVS) is a world leader in the research and development of products to protect and improve health and well-being. The company has core businesses in pharmaceuticals, vaccines, consumer health, generics, eye care and animal health.</h4> </p>

<p><h4>Headquartered in Basel, Switzerland, Novartis employs nearly 115 000 people in over 140 countries worldwide to help save lives and improve the quality of life. The Group is present in India through Novartis India Limited, listed on the Mumbai Stock Exchange and its wholly owned subsidiaries Novartis Healthcare Private Limited, Sandoz Private Limited and Chiron Behring Vaccines Private Limited. </h4></p>

<p><h4>We work to bring new and better medicines to patients in the shortest possible time.

We prioritize our work by patient need and strong scientific understanding of disease.

We use science-based innovation and new business approaches to improve outcomes for patients.

We combine our scientific expertise with a depth of experience to enhance patient access.

We apply our expertise in science and innovation to society’s biggest health challenges. Responsibility is a core part of our business strategy.

Our portfolio focuses on three leading divisions – Pharmaceuticals, Alcon and Sandoz – that have strong innovation power and global scale.</h4>
</p>

<h2>Working at Novartis</h2>

<p><h4>Novartis has a clear mission, focused strategy and strong culture, all of which we expect will support the creation of value over the long term for our company, our shareholders and society. We recognize that our business depends on the creativity, dedication and performance of our associates. We encourage associates to focus on achievement through collaboration and innovation.

A global healthcare leader, Novartis has one of the most exciting product pipelines in the industry today. A pipeline of innovative medicines brought to life by diverse, talented, performance driven people. All of which makes us one of the most rewarding employers in our field.

Our company culture is guided by high ethical standards. Our values help guide the choices people make every day, and they define our culture and help us execute the Novartis strategy in line with our mission and vision.
    <ul>
    <li>Innovation</li>
    <li>Quality</li>
    <li>Collaboration</li>
    <li>Performance</li>
    <li>Courage</li>
    <li>Integrity</li>
    </ul>
     </h4></p>
</body>
</html>