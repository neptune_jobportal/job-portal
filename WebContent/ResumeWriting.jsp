<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <h1><font color = white>Resume Writting</font></h1>
    </div>
    <div><ul class="nav navbar-nav"></ul></div>
  </div>
</nav>

<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./images/r1.jpg" alt="Chania" width="460" height="345">
      </div>

      <div class="item">
        <img src="./images/r2.jpg" alt="Chania" width="460" height="345">
      </div>
   
      </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
    <p><h3><b> How to Write a Resume</h3></b></p>
    <p><h4>You're a smart job seeker! You know your resume is a key part of getting a new job. That's why you're taking time to learn how to write a resume so you have the best one possible.
This may be your first time writing a resume; or maybe you're updating an old one for the umpteenth time. Either way, you'll want to use this guide to be sure you're doing all you can to have the best resume possible.
I created this online resume workshop so you can use it as a step-by-step guide or as a resource to answer particular resume questions. So feel free to start with Step 1 and work your way through to Step 10. Or, jump around to find answers to your questions.</p>
<p><h4>Key points that will serve as a compass as you go through each step of writing your resume.
<p><b>Step 1: Find a Job for Your Resume</b></p>
<p>Learn why this step is important to writing a good resume. Don't make the mistake so many make by doing this step after they write their resumes.</p>
<p><b>Step 2: List of Keywords for Your Resume</b></p>
<p>Recruiters and employers search for keywords, so you need to put them in your resume if you want to be found.</p>
<p><b>Step 3: Choose a Resume Format</b></p>
<p>One size doesn't fit all when it comes to resume format. Learn which of these three resume formats will make your job search a success.</p>
<p>Chronological Resume Template</p>
<p>Functional Resume Template</p>
<p>Combination Resume Template</p>
<p><b>Step 4: Your Resume Heading</b></p>
<p>Believe it or not, there's a right way and a wrong way to do this easy step. Be sure you do it the right way!</p>
<p><b>Step 5: Your Resume Job Objective</b></p>
<p>Learn the pros and cons of having a job objective statement, how to write a good one, and some good options for not having one. For example, here's a Sample of a Professional Title on a Resume.</p>
<p><b>Step 6: Your Summary of Qualifications</b></p>
<p>If the employer reads only this part of your resume, does she get the very best of what you have to offer? Find out how to make your Summary shine!</p>
<p><b>Step 7: Your Work Experience</b></p>
<p>Know how to write your work history on your resume to make the best of it, even if you have tough problems. In this step, you'll learn:</p>
<p>How to Explain Unemployment on Your Resume</p>
<p>How to Overcome Age Discrimination With Dates on a Resume</p>
<p><b>Step 8: Your Resume Achievement Statements</b></p>
<p>Achievement statements tell the employer you're worth hiring, or at least interviewing for the job. Spend time on this part so you use your resume real estate wisely.</p>
<p><b>Step 9: Listing Education on Your Resume</b></p>
<p>Where to put your Education section, what to list in it, and how to deal with many college degrees or no degree at all.</p>
<p><b>Step 10: Community Service and Other Lists on Your Resume</b></p>
<p>Where and how to put all those lists of community service, skills, and other things that need a place of their own on your resume.</h4></p> 
 
 
 
 
</div>

</body>
</html>
