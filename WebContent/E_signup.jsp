<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<title>Naukri Adda.com</title>
</head>
<body>
	<img src = ./images/Logo.png width = 150px height = 150px>
	<div class="container">
		<div class="col-sm-4"></div>
		<div class="col-sm-4" >
			<form action="E_signupController">
				<table align="center" class="table">
				<thead>
				<tr><th colspan="2" align="center"><h3>Employer Create your account</h3></th></tr></thead>
				<tr><td><b>Email:</b></td><td><input type = email name = email placeholder = "Enter email" required></td></tr>
				<tr><td><b>Password:</b></td><td><input type = password name = password placeholder = "Password" required></td></tr>
				<tr><td><b>Company Name:</b></td><td><input type = text placeholder = "Company Name" name = companyname required></td></tr>
<tr><td>Industry type:</td><td>
				
<select>

	<option value="Accounting">Accounting</option>
	<option value="Advertising">Advertising</option>
	<option value="Aerospace">Aerospace</option>
	<option value="Aircraft">Aircraft</option>
    <option value="Airline">Airline</option>
    <option value="Automotive">Automotive</option>
    <option value="Banking">Banking</option>
    <option value="Broadcasting">Broadcasting</option>
    <option value="Biotechnology">Biotechnology</option>
    <option value="Cargo Handling">Cargo Handling</option>
    <option value="Computer">Computer</option>
    <option value="Consulting">Consulting</option>
    <option value="Cosmetics">Cosmetics</option>
    <option value="Defense">Defense</option>
    <option value="Education">Education</option>
    <option value="Electronics">Electronics</option>
    <option value="Entertainment">Entertainment</option>
    <option value="Executive Search">Executive Search</option>
    <option value="Financial Services">Financial Services</option>
    <option value="Food, Beverage & Tobacco">Food, Beverage & Tobacco</option>
    <option value="Grocery">Grocery</option>
    <option value="Health Care">Health Care</option>
    <option value="Internet Publishing">Internet Publishing</option>
    <option value="Investment Banking">Investment Banking</option>
    <option value="Manufacturing">Manufacturing</option>
    <option value="Music">Music</option>
    <option value="Newspaper Publishers">Newspaper Publishers</option>
    <option value="Pharmaceuticals">Pharmaceuticals</option>
    <option value="Private Equity">Private Equity</option>
    <option value="Publishing">Publishing</option>
    <option value="Real Estate">Real Estate</option>
    <option value="Retail & Wholesale">Retail & Wholesale</option>
    <option value="Securities & Commodity Exchanges">Securities & Commodity Exchanges</option>
    <option value="Service">Service</option>
    <option value="Software">Software</option>
    <option value="Softwaret">Software</option>
    <option value="Technology">Technology</option>
    <option value="Telecommunications">Telecommunications</option>
    <option value="Transportation">Transportation</option>
    <option value="Venture Capital">Venture Capital</option>

</select></td></tr>
				
<tr><td><b>Location:</b></td><td>
	
	<select>
	<option value="Agra">Agra</option>
    <option value="Ahmedabad">Ahmedabad</option>
    <option value="Alwar">Alwar</option>
    <option value="Alappuzha">Alappuzha</option>
    <option value="Amritsar">Amritsar</option>
    <option value="Aurangabad">Aurangabad</option>
    <option value="Banglore">Banglore</option>
    <option value="Bharatpur">Bharatpur</option>
    <option value="Bhikanar">Bhikanar</option>
    <option value="Bhopal">Bhopal</option>
    <option value="Bhubaneshwar">Bhubaneshwar</option>
    <option value="Bodhgaya">Bodhgaya</option>
    <option value="Chandigarh">Chandigarh</option>
    <option value="Chennai">Chennai</option>
    <option value="Coimbatore">Coimbatore</option>
    <option value="Cuttack">Cuttack</option>
    <option value="Dalhousie">Dalhousie</option>
    <option value="Delhi">Delhi</option>
    <option value="Diu-island">Diu-island</option>
    <option value="Ernakulam">Ernakulam</option>
    <option value="Faridabad">Faridabad</option>
    <option value="Gangtok">Gangtok</option>
    <option value="Ghaziabad">Ghaziabad</option>
    <option value="Gurgaon">Gurgaon</option>
    <option value="Guwahati">Guwahati</option>
    <option value="Gwalior">Gwalior</option>
    <option value="Haridwar">Haridwar</option>
    <option value="Hyderabad">Hyderabad</option>
    <option value="Imphal">Imphal</option>
    <option value="Indore">Indore</option>
    <option value="Jabalpur">Jabalpur</option>
    <option value="Jaipur">Jaipur</option>
    <option value="Jaisalmer">Jaisalmer</option>
    <option value="Jalandhar">Jalandhar</option>
    <option value="Jamshedpur">Jamshedpur</option>
    <option value="Jodhpur">Jodhpur</option>
    <option value="Kanpur">Kanpur</option>
    <option value="Kanyakumari">Kanyakumari</option>
    <option value="Kochi">Kochi</option>
    <option value="Kodiakanal">Kodiakanal</option>
    <option value="Kolkata">Kolkata</option>
    <option value="Kota">Kota</option>
    <option value="Kottayam">Kottayam</option>
    <option value="Lucknow">Lucknow</option>
    <option value="Ludhiana">Ludhiana</option>
    <option value="Madurai">Madurai</option>
    <option value="Manali">Manali</option>
    <option value="Mangalore">Mangalore</option>
    <option value="Mathura">Mathura</option>
    <option value="Mount-Abu">Mount-Abu</option>
    <option value="Mumbai">Mumbai</option>
    <option value="Mussoorie">Mussoorie</option>
    <option value="Mysore">Mysore</option>
    <option value="Nagpur">Nagpur</option>
    <option value="Noida">Noida</option>
    <option value="Ooty">Ooty</option>
    <option value="Panaji">Panaji</option>
    <option value="Pondicherry">Pondicherry</option>
    <option value="Porbandar">Porbandar</option>
    <option value="Portblair">Portblair</option>
    <option value="Pune">Pune</option>
    <option value="Puri">Puri</option>
    <option value="Rajkot">Rajkot</option>
    <option value="Rameshwaram">Rameshwaram</option>
    <option value="Ranchi">Ranchi</option>
    <option value="Secunderabad">Secunderabad</option>
    <option value="Shimla">Shimla</option>
    <option value="Surat">Surat</option>
    <option value="Thanjavur">Thanjavur</option>
    <option value="Tiruchchirapalli">Tiruchchirapalli</option>
    <option value="Thissur">Thissur</option>
    <option value="Tirumala">Tirumala</option>
    <option value="Udaipur">Udaipur</option>
    <option value="Vadodra">Vadodra</option>
    <option value="Varanasi">Varanasi</option>
    <option value="Vijayawada">Vijayawada</option>
    <option value="Visakhapatnam">Visakhapatnam</option>
</select></td></tr>
				
				<tr><td><b>Phone:</b></td><td><input type = text name = phone placeholder = "Phone" required></td></tr>
				
<tr><td><b>Nationality:</b></td><td>

	<select>
	<option value="Indian">Indian</option>
	<option value="Italy">Italy</option>
	<option value="United Kingdom">United Kingdom</option>
	<option value="United states of America">United states of America</option>
	<option value="Australia">Australia</option>
	<option value="China">China</option>
</select></td></tr>

				<tr><td align="center"><input type = submit value = Register></td></tr>
				</table>
			</form>
<%
String msg = request.getParameter("msg");
if(msg != null){
	out.print("<table align=center><tr><td><font color=red>*" +  msg + "</font color></td></tr></table>");
}
%>
		</div>
	</div>
</body>
</html>