<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <h1><font color = "white">Interviewing Skills</font></h1>
    </div>
    <div>
      <ul class="nav navbar-nav">
       
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./images/interview1.jpg" alt="Chania" width="460" height="345">
      </div>

      <div class="item">
        <img src="./images/interview2.jpg" alt="Chania" width="460" height="345">
      </div>
   
      <div class="item">
        <img src="./images/interview3.jpeg" alt="Flower" width="460" height="345">
      </div>

     
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
 
  <p><h4>&nbsp&nbsp&nbsp&nbsp&nbspYour qualifications and skills are only a part of what the hiring manager is looking for when conducting an interview. Whether you get the job depends largely on how you behave at the interview what you do, what you say, and how you say it.

From the first door you walk through to the final handshake on your way out, this lesson will help you learn how to behave during an interview. It will help you identify basic business etiquette that can improve your chances, as well as show you how your tone of voice and body language can be interpreted by an interviewer. It will also outline the basic rules for answering questions well.</h4></p>

<h2>Business etiquette</h2>
<p>


<h4>To land the job, you'll to appear professional, confident, and capable. You will be judged not only on your ability to do the job itself but also on how well you will get along with the people with whom you will be working. The hiring manager watches your communication skills and your manners to predict whether you'll work well with others.

Fair or not, using poor manners can give the hiring manager the impression that:

    You are unlikely to be able to perform well in certain work situations, especially those involving work teams or customers.
    You do not care about, value, or respect the people with whom you are interacting.
    You are prone to rudeness or ignorance.</h4>

<h3>Top ten business etiquette tips to use in an interview:</h3>

    <p><h4>1.Be on time. If you'll be late, show that you are respectful of the interviewer's time. Call to say when you'll be arriving.</h4></p>
    <p><h4>2.Thank the interviewer for taking the time to meet with you, both at the beginning of the interview and again at the end.</h4></p>
    <p><h4>3.Dress appropriately. If you arrive dressed too casually or too formally, the situation can be uncomfortable for both you and the person interviewing you.</h4></p>
    <p><h4>4.Introduce yourself to the receptionist and to everyone you meet in the interview.</h4></p>
    <p><h4>5.Shake hands with everyone, using a firm but not forceful grip, and make strong eye contact.</h4></p>
    <p><h4>6.Sit when you are asked to sit, not before.</h4></p>
    <p><h4>7.Place your loose items on the floor next to your seat, in your lap, or on the side table, coffee table, or in front of you at a conference table; do not put them on the interviewer's desk unless it is offered to you. Your briefcase or bag should be kept at your feet, not on a chair or table.</h4></p>
    <p><h4>8.Do not ask for refreshments or permission to smoke. If a refreshment is offered, you may accept. If you are asked to dine out as part of your interview, use good table manners.</h4></p>
    <p><h4>9.Keep all of your mobile and other electronic devices turned completely off. A phone set to vibrate will interrupt the meeting.</h4></p>
    <p><h4>10.Keep a positive and friendly attitude.</h4></p>

</p>
</div>

</body>
</html>